#include <stdio.h>
#include <locale.h>
#include <stdlib.h>

typedef struct No *arvore;

struct No {
    int valor;
    struct No *esq;
    struct No *dir;
};


arvore *cria_arvore() {

    arvore *raiz = (arvore *) malloc(sizeof(arvore));

    if (raiz != NULL)
        *raiz = NULL;

    return raiz;
}

void libera_no(struct No *no) {

    if (no == NULL)
        return;
    libera_no(no->esq);
    libera_no(no->dir);
    free(no);

    // no = NULL;
}

void libera_arvore(arvore *raiz) {

    if (raiz == NULL)
        return;

    libera_no(*raiz);
    free(raiz);

}

int is_empty(arvore *raiz) {

    if (raiz == NULL || *raiz == NULL) {
        return 1;
    }

    return 0;

}

int altura_arvore(arvore *raiz) {

    if (raiz == NULL || *raiz == NULL)
        return 0;

    int altura_esq = altura_arvore(&((*raiz)->esq));
    int altura_dir = altura_arvore(&((*raiz)->dir));

    return altura_esq > altura_dir ? altura_esq + 1 : altura_dir + 1;
}

int total_nos_arvore(arvore *raiz) {

    if (raiz == NULL || *raiz == NULL)
        return 0;
    int total_nos_esq = total_nos_arvore(&((*raiz)->esq));
    int total_nos_dir = total_nos_arvore(&((*raiz)->dir) + 1);

    return total_nos_esq + total_nos_dir;
}

int percorre_arvore(struct No *no) {

    while (no->esq != NULL && no->dir != NULL) {
        no->esq = no->esq = no->esq;
        no->dir = no->dir = no->dir;
    }

    return 0;
}


void percorre_arvore_preOrdem(arvore *raiz) {

    if (raiz == NULL) return;

    if (*raiz != NULL) {
        printf("%d\t", (*raiz)->valor);
        percorre_arvore_preOrdem(&((*raiz)->esq));
        percorre_arvore_preOrdem(&((*raiz)->dir));
    }
}

void percorre_arvore_ordem(arvore *raiz) {

    if (raiz == NULL) return;

    if (*raiz != NULL) {
        percorre_arvore_preOrdem(&((*raiz)->esq));
        printf("%d\t", (*raiz)->valor);
        percorre_arvore_preOrdem(&((*raiz)->dir));
    }
}

void percorre_arvore_posOrdem(arvore *raiz) {

    if (raiz == NULL) return;

    if (*raiz != NULL) {
        percorre_arvore_preOrdem(&((*raiz)->esq));
        percorre_arvore_preOrdem(&((*raiz)->dir));
        printf("%d\t", (*raiz)->valor);
    }
}

void qualOrdemPercorrer(int i, arvore *raiz) {

    printf("\n");
    if (i == 1)
        percorre_arvore_preOrdem(raiz);
    if (i == 2)
        percorre_arvore_ordem(raiz);
    if (i == 3)
        percorre_arvore_posOrdem(raiz);

}

int inserir(arvore *raiz, int valor) {
    struct No *novo;
    novo = (struct No *) malloc(sizeof(struct No));

    if (raiz == NULL || novo == NULL) return 0;

    novo->valor = valor;
    novo->esq = NULL;
    novo->dir = NULL;

    if (*raiz == NULL) *raiz = novo;
    else {
        struct No *atual = *raiz;
        struct No *anterior = NULL;

        while (atual != NULL) {

            anterior = atual;
            if (valor == atual->valor) {
                free(novo);
                return 0; // elemento j� existe
            }

            if (valor > atual->valor) atual = atual->dir;
            else atual = atual->esq;
        }

        if (valor < anterior->valor) anterior->esq = novo;
        else anterior->dir = novo;
    }

    return 1;
}

void informacaoes_arvore(arvore *raiz) {

    printf("\n�rvore vazia: %d\tAltura da �rvore: %d\tTotal n�s: %d", is_empty(raiz), altura_arvore(raiz), total_nos_arvore(raiz));

}

int main() {
    setlocale(LC_ALL, "Portuguese");
    printf("Hello, mund�o!\n");

    arvore *raiz = cria_arvore();
    informacaoes_arvore(raiz);

    int valores[6] = {5, 3, 8, 4, 41, 6};
    printf("\nValores no vetor.\n");
    for (int i = 0; i < 6; i++) printf("%d\t", valores[i]);

    for (int i = 0; i < 6; i++) inserir(raiz, valores[i]);

    informacaoes_arvore(raiz);

    libera_arvore(raiz);
    qualOrdemPercorrer(1, raiz);
    // qualOrdemPercorrer(2, raiz);
    // qualOrdemPercorrer(3, raiz);

    return 0;
}
